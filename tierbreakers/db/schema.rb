# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150302214836) do

  create_table "brackets", force: true do |t|
    t.integer  "player_id"
    t.integer  "bracket_round"
    t.integer  "event_id"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "brackets", ["event_id"], name: "index_brackets_on_event_id"
  add_index "brackets", ["player_id"], name: "index_brackets_on_player_id"

  create_table "events", force: true do |t|
    t.string   "event_name"
    t.datetime "event_date"
    t.integer  "venue_id"
    t.string   "event_description"
    t.string   "event_image"
    t.integer  "tournament_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "events", ["tournament_id"], name: "index_events_on_tournament_id"

  create_table "events_games", force: true do |t|
    t.integer  "game_id"
    t.integer  "event_id"
    t.integer  "max_players"
    t.integer  "max_teams"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "events_games", ["event_id"], name: "index_events_games_on_event_id"
  add_index "events_games", ["game_id"], name: "index_events_games_on_game_id"

  create_table "events_teams", force: true do |t|
    t.integer  "team_id"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "events_teams", ["event_id"], name: "index_events_teams_on_event_id"
  add_index "events_teams", ["team_id"], name: "index_events_teams_on_team_id"

  create_table "game_characters", force: true do |t|
    t.string   "character_name"
    t.string   "character_image"
    t.integer  "game_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "game_characters", ["game_id"], name: "index_game_characters_on_game_id"

  create_table "games", force: true do |t|
    t.string   "game_name"
    t.string   "platform"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "games_players", force: true do |t|
    t.integer  "game_id"
    t.integer  "player_id"
    t.integer  "game_character_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "games_players", ["game_character_id"], name: "index_games_players_on_game_character_id"
  add_index "games_players", ["game_id"], name: "index_games_players_on_game_id"
  add_index "games_players", ["player_id"], name: "index_games_players_on_player_id"

  create_table "players", force: true do |t|
    t.string   "player_first_name"
    t.string   "player_last_name"
    t.string   "player_email"
    t.string   "player_phone"
    t.string   "player_login"
    t.string   "player_password"
    t.string   "player_address"
    t.string   "player_state"
    t.string   "player_city"
    t.string   "player_zip"
    t.string   "player_image"
    t.string   "player_tag"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "players_round_robins", force: true do |t|
    t.integer  "player_id"
    t.integer  "round_robin_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "players_round_robins", ["player_id"], name: "index_players_round_robins_on_player_id"
  add_index "players_round_robins", ["round_robin_id"], name: "index_players_round_robins_on_round_robin_id"

  create_table "round_robins", force: true do |t|
    t.integer  "team_id"
    t.integer  "player_id"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "round_robins", ["event_id"], name: "index_round_robins_on_event_id"
  add_index "round_robins", ["player_id"], name: "index_round_robins_on_player_id"
  add_index "round_robins", ["team_id"], name: "index_round_robins_on_team_id"

  create_table "team_players", force: true do |t|
    t.integer  "team_id"
    t.integer  "player_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "team_players", ["player_id"], name: "index_team_players_on_player_id"
  add_index "team_players", ["team_id"], name: "index_team_players_on_team_id"

  create_table "teams", force: true do |t|
    t.string   "team_name"
    t.string   "team_image"
    t.integer  "player_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "teams", ["player_id"], name: "index_teams_on_player_id"

  create_table "teams_tournaments", force: true do |t|
    t.integer  "team_id"
    t.integer  "tournament_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "teams_tournaments", ["team_id"], name: "index_teams_tournaments_on_team_id"
  add_index "teams_tournaments", ["tournament_id"], name: "index_teams_tournaments_on_tournament_id"

  create_table "tournaments", force: true do |t|
    t.string   "tournament_name"
    t.string   "tournament_state"
    t.string   "tournamentcol_city"
    t.integer  "tournament_max_players"
    t.datetime "tournament_start_date"
    t.datetime "tournament_end_date"
    t.datetime "tournament_reg_start"
    t.datetime "tournament_reg_end"
    t.string   "tournament_description"
    t.integer  "tournament_game_id"
    t.string   "tournament_image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "venues", force: true do |t|
    t.string   "venue_name"
    t.string   "venue_address"
    t.string   "venue_state"
    t.string   "venue_city"
    t.string   "venue_zip"
    t.integer  "venue_max_people"
    t.string   "venue_description"
    t.string   "venue_room"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
