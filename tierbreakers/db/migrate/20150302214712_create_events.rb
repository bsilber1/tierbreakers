class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :event_name
      t.datetime :event_date
      t.integer :venue_id
      t.string :event_description
      t.string :event_image
      t.belongs_to :tournament, index: true

      t.timestamps
    end
  end
end
