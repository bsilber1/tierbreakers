class CreateTournaments < ActiveRecord::Migration
  def change
    create_table :tournaments do |t|
      t.string :tournament_name
      t.string :tournament_state
      t.string :tournamentcol_city
      t.integer :tournament_max_players
      t.datetime :tournament_start_date
      t.datetime :tournament_end_date
      t.datetime :tournament_reg_start
      t.datetime :tournament_reg_end
      t.string :tournament_description
      t.integer :tournament_game_id
      t.string :tournament_image

      t.timestamps
    end
  end
end
