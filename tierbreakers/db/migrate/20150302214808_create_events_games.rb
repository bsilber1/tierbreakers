class CreateEventsGames < ActiveRecord::Migration
  def change
    create_table :events_games do |t|
      t.belongs_to :game, index: true
      t.belongs_to :event, index: true
      t.integer :max_players
      t.integer :max_teams

      t.timestamps
    end
  end
end
