class CreateTeamPlayers < ActiveRecord::Migration
  def change
    create_table :team_players do |t|
      t.belongs_to :team, index: true
      t.belongs_to :player, index: true

      t.timestamps
    end
  end
end
