class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.string :venue_name
      t.string :venue_address
      t.string :venue_state
      t.string :venue_city
      t.string :venue_zip
      t.integer :venue_max_people
      t.string :venue_description
      t.string :venue_room

      t.timestamps
    end
  end
end
