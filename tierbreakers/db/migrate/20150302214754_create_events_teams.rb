class CreateEventsTeams < ActiveRecord::Migration
  def change
    create_table :events_teams do |t|
      t.belongs_to :team, index: true
      t.belongs_to :event, index: true

      t.timestamps
    end
  end
end
