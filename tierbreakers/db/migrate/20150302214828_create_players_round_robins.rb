class CreatePlayersRoundRobins < ActiveRecord::Migration
  def change
    create_table :players_round_robins do |t|
      t.belongs_to :player, index: true
      t.belongs_to :player, index: true
      t.belongs_to :player, index: true
      t.belongs_to :round_robin, index: true
      t.belongs_to :player, index: true
      t.belongs_to :player, index: true
      t.belongs_to :player, index: true

      t.timestamps
    end
  end
end
