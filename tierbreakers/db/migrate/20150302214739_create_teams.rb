class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :team_name
      t.string :team_image
      t.belongs_to :player, index: true

      t.timestamps
    end
  end
end
