class CreateGameCharacters < ActiveRecord::Migration
  def change
    create_table :game_characters do |t|
      t.string :character_name
      t.string :character_image
      t.belongs_to :game, index: true

      t.timestamps
    end
  end
end
