class CreateTeamsTournaments < ActiveRecord::Migration
  def change
    create_table :teams_tournaments do |t|
      t.belongs_to :team, index: true
      t.belongs_to :tournament, index: true

      t.timestamps
    end
  end
end
