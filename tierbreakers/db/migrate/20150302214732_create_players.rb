class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :player_first_name
      t.string :player_last_name
      t.string :player_email
      t.string :player_phone
      t.string :player_login
      t.string :player_password
      t.string :player_address
      t.string :player_state
      t.string :player_city
      t.string :player_zip
      t.string :player_image
      t.string :player_tag

      t.timestamps
    end
  end
end
