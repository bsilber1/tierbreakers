class CreateBrackets < ActiveRecord::Migration
  def change
    create_table :brackets do |t|
      t.belongs_to :player, index: true
      t.belongs_to :player, index: true
      t.integer :bracket_round
      t.belongs_to :player, index: true
      t.belongs_to :event, index: true
      t.string :type
      t.belongs_to :player, index: true
      t.belongs_to :player, index: true
      t.belongs_to :player, index: true
      t.belongs_to :player, index: true
      t.belongs_to :player, index: true

      t.timestamps
    end
  end
end
