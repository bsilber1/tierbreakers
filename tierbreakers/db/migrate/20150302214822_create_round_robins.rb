class CreateRoundRobins < ActiveRecord::Migration
  def change
    create_table :round_robins do |t|
      t.belongs_to :team, index: true
      t.belongs_to :team, index: true
      t.belongs_to :player, index: true
      t.belongs_to :event, index: true

      t.timestamps
    end
  end
end
