class CreateGamesPlayers < ActiveRecord::Migration
  def change
    create_table :games_players do |t|
      t.belongs_to :game, index: true
      t.belongs_to :player, index: true
      t.belongs_to :game_character, index: true
      t.belongs_to :game_character, index: true
      t.belongs_to :game_character, index: true

      t.timestamps
    end
  end
end
