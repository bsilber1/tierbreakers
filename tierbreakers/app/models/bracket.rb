class Bracket < ActiveRecord::Base
  belongs_to :player, :class_name => "Player1", :foreign_key => "player1_id"
  belongs_to :player, :class_name => "Player2", :foreign_key => "player2_id"
  belongs_to :player, :class_name => "Winner", :foreign_key => "winner_id"
  belongs_to :event
  belongs_to :player, :class_name => "Match1", :foreign_key => "match1_id"
  belongs_to :player, :class_name => "Match2", :foreign_key => "match2_id"
  belongs_to :player, :class_name => "Match3", :foreign_key => "match3_id"
  belongs_to :player, :class_name => "Match4", :foreign_key => "match4_id"
  belongs_to :player, :class_name => "Match5", :foreign_key => "match5_id"
end
