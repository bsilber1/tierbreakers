class Event < ActiveRecord::Base
   belongs_to :tournament
   belongs_to :venue
   accepts_nested_attributes_for :venue
   accepts_nested_attributes_for :tournament 
   has_and_belongs_to_many :teams 
   has_and_belongs_to_many :games 
   has_and_belongs_to_many :players 
   has_many :round_robins, dependent: :destroy 
end
