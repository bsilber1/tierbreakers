class EventsGame < ActiveRecord::Base
  belongs_to :game
  belongs_to :event
  accepts_nested_attributes_for :game
  accepts_nested_attributes_for :event 
end
