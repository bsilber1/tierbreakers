class EventsTeam < ActiveRecord::Base
  belongs_to :team
  belongs_to :event
  accepts_nested_attributes_for :team
  accepts_nested_attributes_for :event 
end
