class EventsGamesController < ApplicationController
  before_action :set_events_game, only: [:show, :edit, :update, :destroy]

  # GET /events_games
  # GET /events_games.json
  def index
    @events_games = EventsGame.all
  end

  # GET /events_games/1
  # GET /events_games/1.json
  def show
  end

  # GET /events_games/new
  def new
    @events_game = EventsGame.new
  end

  # GET /events_games/1/edit
  def edit
  end

  # POST /events_games
  # POST /events_games.json
  def create
    @events_game = EventsGame.new(events_game_params)

    respond_to do |format|
      if @events_game.save
        format.html { redirect_to @events_game, notice: 'Events game was successfully created.' }
        format.json { render :show, status: :created, location: @events_game }
      else
        format.html { render :new }
        format.json { render json: @events_game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events_games/1
  # PATCH/PUT /events_games/1.json
  def update
    respond_to do |format|
      if @events_game.update(events_game_params)
        format.html { redirect_to @events_game, notice: 'Events game was successfully updated.' }
        format.json { render :show, status: :ok, location: @events_game }
      else
        format.html { render :edit }
        format.json { render json: @events_game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events_games/1
  # DELETE /events_games/1.json
  def destroy
    @events_game.destroy
    respond_to do |format|
      format.html { redirect_to events_games_url, notice: 'Events game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_events_game
      @events_game = EventsGame.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def events_game_params
      params.require(:events_game).permit(:game_id, :event_id, :max_players, :max_teams)
    end
end
