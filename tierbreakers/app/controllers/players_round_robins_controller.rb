class PlayersRoundRobinsController < ApplicationController
  before_action :set_players_round_robin, only: [:show, :edit, :update, :destroy]

  # GET /players_round_robins
  # GET /players_round_robins.json
  def index
    @players_round_robins = PlayersRoundRobin.all
  end

  # GET /players_round_robins/1
  # GET /players_round_robins/1.json
  def show
  end

  # GET /players_round_robins/new
  def new
    @players_round_robin = PlayersRoundRobin.new
  end

  # GET /players_round_robins/1/edit
  def edit
  end

  # POST /players_round_robins
  # POST /players_round_robins.json
  def create
    @players_round_robin = PlayersRoundRobin.new(players_round_robin_params)

    respond_to do |format|
      if @players_round_robin.save
        format.html { redirect_to @players_round_robin, notice: 'Players round robin was successfully created.' }
        format.json { render :show, status: :created, location: @players_round_robin }
      else
        format.html { render :new }
        format.json { render json: @players_round_robin.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /players_round_robins/1
  # PATCH/PUT /players_round_robins/1.json
  def update
    respond_to do |format|
      if @players_round_robin.update(players_round_robin_params)
        format.html { redirect_to @players_round_robin, notice: 'Players round robin was successfully updated.' }
        format.json { render :show, status: :ok, location: @players_round_robin }
      else
        format.html { render :edit }
        format.json { render json: @players_round_robin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /players_round_robins/1
  # DELETE /players_round_robins/1.json
  def destroy
    @players_round_robin.destroy
    respond_to do |format|
      format.html { redirect_to players_round_robins_url, notice: 'Players round robin was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_players_round_robin
      @players_round_robin = PlayersRoundRobin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def players_round_robin_params
      params.require(:players_round_robin).permit(:player_id, :player_id, :player_id, :round_robin_id, :player_id, :player_id, :player_id)
    end
end
