class EventsTeamsController < ApplicationController
  before_action :set_events_team, only: [:show, :edit, :update, :destroy]

  # GET /events_teams
  # GET /events_teams.json
  def index
    @events_teams = EventsTeam.all
  end

  # GET /events_teams/1
  # GET /events_teams/1.json
  def show
  end

  # GET /events_teams/new
  def new
    @events_team = EventsTeam.new
  end

  # GET /events_teams/1/edit
  def edit
  end

  # POST /events_teams
  # POST /events_teams.json
  def create
    @events_team = EventsTeam.new(events_team_params)

    respond_to do |format|
      if @events_team.save
        format.html { redirect_to @events_team, notice: 'Events team was successfully created.' }
        format.json { render :show, status: :created, location: @events_team }
      else
        format.html { render :new }
        format.json { render json: @events_team.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events_teams/1
  # PATCH/PUT /events_teams/1.json
  def update
    respond_to do |format|
      if @events_team.update(events_team_params)
        format.html { redirect_to @events_team, notice: 'Events team was successfully updated.' }
        format.json { render :show, status: :ok, location: @events_team }
      else
        format.html { render :edit }
        format.json { render json: @events_team.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events_teams/1
  # DELETE /events_teams/1.json
  def destroy
    @events_team.destroy
    respond_to do |format|
      format.html { redirect_to events_teams_url, notice: 'Events team was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_events_team
      @events_team = EventsTeam.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def events_team_params
      params.require(:events_team).permit(:team_id, :event_id)
    end
end
