class TeamsTournamentsController < ApplicationController
  before_action :set_teams_tournament, only: [:show, :edit, :update, :destroy]

  # GET /teams_tournaments
  # GET /teams_tournaments.json
  def index
    @teams_tournaments = TeamsTournament.all
  end

  # GET /teams_tournaments/1
  # GET /teams_tournaments/1.json
  def show
  end

  # GET /teams_tournaments/new
  def new
    @teams_tournament = TeamsTournament.new
  end

  # GET /teams_tournaments/1/edit
  def edit
  end

  # POST /teams_tournaments
  # POST /teams_tournaments.json
  def create
    @teams_tournament = TeamsTournament.new(teams_tournament_params)

    respond_to do |format|
      if @teams_tournament.save
        format.html { redirect_to @teams_tournament, notice: 'Teams tournament was successfully created.' }
        format.json { render :show, status: :created, location: @teams_tournament }
      else
        format.html { render :new }
        format.json { render json: @teams_tournament.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /teams_tournaments/1
  # PATCH/PUT /teams_tournaments/1.json
  def update
    respond_to do |format|
      if @teams_tournament.update(teams_tournament_params)
        format.html { redirect_to @teams_tournament, notice: 'Teams tournament was successfully updated.' }
        format.json { render :show, status: :ok, location: @teams_tournament }
      else
        format.html { render :edit }
        format.json { render json: @teams_tournament.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teams_tournaments/1
  # DELETE /teams_tournaments/1.json
  def destroy
    @teams_tournament.destroy
    respond_to do |format|
      format.html { redirect_to teams_tournaments_url, notice: 'Teams tournament was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_teams_tournament
      @teams_tournament = TeamsTournament.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def teams_tournament_params
      params.require(:teams_tournament).permit(:team_id, :tournament_id)
    end
end
