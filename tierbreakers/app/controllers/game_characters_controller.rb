class GameCharactersController < ApplicationController
  before_action :set_game_character, only: [:show, :edit, :update, :destroy]

  # GET /game_characters
  # GET /game_characters.json
  def index
    @game_characters = GameCharacter.all
  end

  # GET /game_characters/1
  # GET /game_characters/1.json
  def show
  end

  # GET /game_characters/new
  def new
    @game_character = GameCharacter.new
  end

  # GET /game_characters/1/edit
  def edit
  end

  # POST /game_characters
  # POST /game_characters.json
  def create
    @game_character = GameCharacter.new(game_character_params)

    respond_to do |format|
      if @game_character.save
        format.html { redirect_to @game_character, notice: 'Game character was successfully created.' }
        format.json { render :show, status: :created, location: @game_character }
      else
        format.html { render :new }
        format.json { render json: @game_character.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /game_characters/1
  # PATCH/PUT /game_characters/1.json
  def update
    respond_to do |format|
      if @game_character.update(game_character_params)
        format.html { redirect_to @game_character, notice: 'Game character was successfully updated.' }
        format.json { render :show, status: :ok, location: @game_character }
      else
        format.html { render :edit }
        format.json { render json: @game_character.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /game_characters/1
  # DELETE /game_characters/1.json
  def destroy
    @game_character.destroy
    respond_to do |format|
      format.html { redirect_to game_characters_url, notice: 'Game character was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game_character
      @game_character = GameCharacter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_character_params
      params.require(:game_character).permit(:character_name, :character_image, :game_id)
    end
end
