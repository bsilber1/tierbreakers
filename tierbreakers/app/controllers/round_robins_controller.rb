class RoundRobinsController < ApplicationController
  before_action :set_round_robin, only: [:show, :edit, :update, :destroy]

  # GET /round_robins
  # GET /round_robins.json
  def index
    @round_robins = RoundRobin.all
  end

  # GET /round_robins/1
  # GET /round_robins/1.json
  def show
  end

  # GET /round_robins/new
  def new
    @round_robin = RoundRobin.new
  end

  # GET /round_robins/1/edit
  def edit
  end

  # POST /round_robins
  # POST /round_robins.json
  def create
    @round_robin = RoundRobin.new(round_robin_params)

    respond_to do |format|
      if @round_robin.save
        format.html { redirect_to @round_robin, notice: 'Round robin was successfully created.' }
        format.json { render :show, status: :created, location: @round_robin }
      else
        format.html { render :new }
        format.json { render json: @round_robin.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /round_robins/1
  # PATCH/PUT /round_robins/1.json
  def update
    respond_to do |format|
      if @round_robin.update(round_robin_params)
        format.html { redirect_to @round_robin, notice: 'Round robin was successfully updated.' }
        format.json { render :show, status: :ok, location: @round_robin }
      else
        format.html { render :edit }
        format.json { render json: @round_robin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /round_robins/1
  # DELETE /round_robins/1.json
  def destroy
    @round_robin.destroy
    respond_to do |format|
      format.html { redirect_to round_robins_url, notice: 'Round robin was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_round_robin
      @round_robin = RoundRobin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def round_robin_params
      params.require(:round_robin).permit(:team_id, :team_id, :player_id, :event_id)
    end
end
