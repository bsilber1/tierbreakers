json.array!(@teams_tournaments) do |teams_tournament|
  json.extract! teams_tournament, :id, :team_id, :tournament_id
  json.url teams_tournament_url(teams_tournament, format: :json)
end
