json.array!(@round_robins) do |round_robin|
  json.extract! round_robin, :id, :team_id, :team_id, :player_id, :event_id
  json.url round_robin_url(round_robin, format: :json)
end
