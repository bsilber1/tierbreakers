json.array!(@players) do |player|
  json.extract! player, :id, :player_first_name, :player_last_name, :player_email, :player_phone, :player_login, :player_password, :player_address, :player_state, :player_city, :player_zip, :player_image, :player_tag
  json.url player_url(player, format: :json)
end
