json.array!(@venues) do |venue|
  json.extract! venue, :id, :venue_name, :venue_address, :venue_state, :venue_city, :venue_zip, :venue_max_people, :venue_description, :venue_room
  json.url venue_url(venue, format: :json)
end
