json.array!(@events) do |event|
  json.extract! event, :id, :event_name, :event_date, :venue_id, :event_description, :event_image, :tournament_id
  json.url event_url(event, format: :json)
end
