json.array!(@events_teams) do |events_team|
  json.extract! events_team, :id, :team_id, :event_id
  json.url events_team_url(events_team, format: :json)
end
