json.array!(@game_characters) do |game_character|
  json.extract! game_character, :id, :character_name, :character_image, :game_id
  json.url game_character_url(game_character, format: :json)
end
