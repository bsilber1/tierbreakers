json.array!(@games) do |game|
  json.extract! game, :id, :game_name, :platform
  json.url game_url(game, format: :json)
end
