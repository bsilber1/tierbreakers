json.array!(@events_games) do |events_game|
  json.extract! events_game, :id, :game_id, :event_id, :max_players, :max_teams
  json.url events_game_url(events_game, format: :json)
end
