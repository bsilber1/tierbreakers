require 'test_helper'

class PlayersControllerTest < ActionController::TestCase
  setup do
    @player = players(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:players)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create player" do
    assert_difference('Player.count') do
      post :create, player: { player_address: @player.player_address, player_city: @player.player_city, player_email: @player.player_email, player_first_name: @player.player_first_name, player_image: @player.player_image, player_last_name: @player.player_last_name, player_login: @player.player_login, player_password: @player.player_password, player_phone: @player.player_phone, player_state: @player.player_state, player_tag: @player.player_tag, player_zip: @player.player_zip }
    end

    assert_redirected_to player_path(assigns(:player))
  end

  test "should show player" do
    get :show, id: @player
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @player
    assert_response :success
  end

  test "should update player" do
    patch :update, id: @player, player: { player_address: @player.player_address, player_city: @player.player_city, player_email: @player.player_email, player_first_name: @player.player_first_name, player_image: @player.player_image, player_last_name: @player.player_last_name, player_login: @player.player_login, player_password: @player.player_password, player_phone: @player.player_phone, player_state: @player.player_state, player_tag: @player.player_tag, player_zip: @player.player_zip }
    assert_redirected_to player_path(assigns(:player))
  end

  test "should destroy player" do
    assert_difference('Player.count', -1) do
      delete :destroy, id: @player
    end

    assert_redirected_to players_path
  end
end
