require 'test_helper'

class RoundRobinsControllerTest < ActionController::TestCase
  setup do
    @round_robin = round_robins(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:round_robins)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create round_robin" do
    assert_difference('RoundRobin.count') do
      post :create, round_robin: { event_id: @round_robin.event_id, player_id: @round_robin.player_id, team_id: @round_robin.team_id, team_id: @round_robin.team_id }
    end

    assert_redirected_to round_robin_path(assigns(:round_robin))
  end

  test "should show round_robin" do
    get :show, id: @round_robin
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @round_robin
    assert_response :success
  end

  test "should update round_robin" do
    patch :update, id: @round_robin, round_robin: { event_id: @round_robin.event_id, player_id: @round_robin.player_id, team_id: @round_robin.team_id, team_id: @round_robin.team_id }
    assert_redirected_to round_robin_path(assigns(:round_robin))
  end

  test "should destroy round_robin" do
    assert_difference('RoundRobin.count', -1) do
      delete :destroy, id: @round_robin
    end

    assert_redirected_to round_robins_path
  end
end
