require 'test_helper'

class EventsTeamsControllerTest < ActionController::TestCase
  setup do
    @events_team = events_teams(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:events_teams)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create events_team" do
    assert_difference('EventsTeam.count') do
      post :create, events_team: { event_id: @events_team.event_id, team_id: @events_team.team_id }
    end

    assert_redirected_to events_team_path(assigns(:events_team))
  end

  test "should show events_team" do
    get :show, id: @events_team
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @events_team
    assert_response :success
  end

  test "should update events_team" do
    patch :update, id: @events_team, events_team: { event_id: @events_team.event_id, team_id: @events_team.team_id }
    assert_redirected_to events_team_path(assigns(:events_team))
  end

  test "should destroy events_team" do
    assert_difference('EventsTeam.count', -1) do
      delete :destroy, id: @events_team
    end

    assert_redirected_to events_teams_path
  end
end
