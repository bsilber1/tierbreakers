require 'test_helper'

class GameCharactersControllerTest < ActionController::TestCase
  setup do
    @game_character = game_characters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:game_characters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create game_character" do
    assert_difference('GameCharacter.count') do
      post :create, game_character: { character_image: @game_character.character_image, character_name: @game_character.character_name, game_id: @game_character.game_id }
    end

    assert_redirected_to game_character_path(assigns(:game_character))
  end

  test "should show game_character" do
    get :show, id: @game_character
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @game_character
    assert_response :success
  end

  test "should update game_character" do
    patch :update, id: @game_character, game_character: { character_image: @game_character.character_image, character_name: @game_character.character_name, game_id: @game_character.game_id }
    assert_redirected_to game_character_path(assigns(:game_character))
  end

  test "should destroy game_character" do
    assert_difference('GameCharacter.count', -1) do
      delete :destroy, id: @game_character
    end

    assert_redirected_to game_characters_path
  end
end
