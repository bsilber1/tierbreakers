require 'test_helper'

class PlayersRoundRobinsControllerTest < ActionController::TestCase
  setup do
    @players_round_robin = players_round_robins(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:players_round_robins)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create players_round_robin" do
    assert_difference('PlayersRoundRobin.count') do
      post :create, players_round_robin: { player_id: @players_round_robin.player_id, player_id: @players_round_robin.player_id, player_id: @players_round_robin.player_id, player_id: @players_round_robin.player_id, player_id: @players_round_robin.player_id, player_id: @players_round_robin.player_id, round_robin_id: @players_round_robin.round_robin_id }
    end

    assert_redirected_to players_round_robin_path(assigns(:players_round_robin))
  end

  test "should show players_round_robin" do
    get :show, id: @players_round_robin
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @players_round_robin
    assert_response :success
  end

  test "should update players_round_robin" do
    patch :update, id: @players_round_robin, players_round_robin: { player_id: @players_round_robin.player_id, player_id: @players_round_robin.player_id, player_id: @players_round_robin.player_id, player_id: @players_round_robin.player_id, player_id: @players_round_robin.player_id, player_id: @players_round_robin.player_id, round_robin_id: @players_round_robin.round_robin_id }
    assert_redirected_to players_round_robin_path(assigns(:players_round_robin))
  end

  test "should destroy players_round_robin" do
    assert_difference('PlayersRoundRobin.count', -1) do
      delete :destroy, id: @players_round_robin
    end

    assert_redirected_to players_round_robins_path
  end
end
