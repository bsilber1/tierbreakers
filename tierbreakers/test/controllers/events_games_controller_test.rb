require 'test_helper'

class EventsGamesControllerTest < ActionController::TestCase
  setup do
    @events_game = events_games(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:events_games)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create events_game" do
    assert_difference('EventsGame.count') do
      post :create, events_game: { event_id: @events_game.event_id, game_id: @events_game.game_id, max_players: @events_game.max_players, max_teams: @events_game.max_teams }
    end

    assert_redirected_to events_game_path(assigns(:events_game))
  end

  test "should show events_game" do
    get :show, id: @events_game
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @events_game
    assert_response :success
  end

  test "should update events_game" do
    patch :update, id: @events_game, events_game: { event_id: @events_game.event_id, game_id: @events_game.game_id, max_players: @events_game.max_players, max_teams: @events_game.max_teams }
    assert_redirected_to events_game_path(assigns(:events_game))
  end

  test "should destroy events_game" do
    assert_difference('EventsGame.count', -1) do
      delete :destroy, id: @events_game
    end

    assert_redirected_to events_games_path
  end
end
