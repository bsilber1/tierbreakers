require 'test_helper'

class TeamsTournamentsControllerTest < ActionController::TestCase
  setup do
    @teams_tournament = teams_tournaments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:teams_tournaments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create teams_tournament" do
    assert_difference('TeamsTournament.count') do
      post :create, teams_tournament: { team_id: @teams_tournament.team_id, tournament_id: @teams_tournament.tournament_id }
    end

    assert_redirected_to teams_tournament_path(assigns(:teams_tournament))
  end

  test "should show teams_tournament" do
    get :show, id: @teams_tournament
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @teams_tournament
    assert_response :success
  end

  test "should update teams_tournament" do
    patch :update, id: @teams_tournament, teams_tournament: { team_id: @teams_tournament.team_id, tournament_id: @teams_tournament.tournament_id }
    assert_redirected_to teams_tournament_path(assigns(:teams_tournament))
  end

  test "should destroy teams_tournament" do
    assert_difference('TeamsTournament.count', -1) do
      delete :destroy, id: @teams_tournament
    end

    assert_redirected_to teams_tournaments_path
  end
end
