require 'test_helper'

class TournamentsControllerTest < ActionController::TestCase
  setup do
    @tournament = tournaments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tournaments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tournament" do
    assert_difference('Tournament.count') do
      post :create, tournament: { tournament_description: @tournament.tournament_description, tournament_end_date: @tournament.tournament_end_date, tournament_game_id: @tournament.tournament_game_id, tournament_image: @tournament.tournament_image, tournament_max_players: @tournament.tournament_max_players, tournament_name: @tournament.tournament_name, tournament_reg_end: @tournament.tournament_reg_end, tournament_reg_start: @tournament.tournament_reg_start, tournament_start_date: @tournament.tournament_start_date, tournament_state: @tournament.tournament_state, tournamentcol_city: @tournament.tournamentcol_city }
    end

    assert_redirected_to tournament_path(assigns(:tournament))
  end

  test "should show tournament" do
    get :show, id: @tournament
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tournament
    assert_response :success
  end

  test "should update tournament" do
    patch :update, id: @tournament, tournament: { tournament_description: @tournament.tournament_description, tournament_end_date: @tournament.tournament_end_date, tournament_game_id: @tournament.tournament_game_id, tournament_image: @tournament.tournament_image, tournament_max_players: @tournament.tournament_max_players, tournament_name: @tournament.tournament_name, tournament_reg_end: @tournament.tournament_reg_end, tournament_reg_start: @tournament.tournament_reg_start, tournament_start_date: @tournament.tournament_start_date, tournament_state: @tournament.tournament_state, tournamentcol_city: @tournament.tournamentcol_city }
    assert_redirected_to tournament_path(assigns(:tournament))
  end

  test "should destroy tournament" do
    assert_difference('Tournament.count', -1) do
      delete :destroy, id: @tournament
    end

    assert_redirected_to tournaments_path
  end
end
